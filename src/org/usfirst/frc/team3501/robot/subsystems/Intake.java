package org.usfirst.frc.team3501.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;

/***
 *
 *
 * @author Meeta
 *
 */
public class Intake extends Subsystem {
  public Intake() {

  }

  /**
   * Runs the intake continuously
   */
  public void RunContinous() {

  }

  /**
   * Starts running the intake for a specific period of time that the user
   * inputs.
   *
   * @param timeToMove
   *          in seconds
   */
  public void RunIntake(double timeToMove) {

  }

  /**
   * Stops the intake
   */
  public void StopIntake() {

  }

  @Override
  protected void initDefaultCommand() {
    // TODO Auto-generated method stub

  }

}
