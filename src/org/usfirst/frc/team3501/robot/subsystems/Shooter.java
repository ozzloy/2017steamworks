package org.usfirst.frc.team3501.robot.subsystems;

public class Shooter {
	public Shooter() {

	}

	/**
	 * Stops fly wheel
	 */
	public void stopFlywheel() {

	}

	/**
	 * Runs the fly wheel at a given speed in () for input time in seconds
	 *
	 * @param speed
	 *            in ()
	 * @param time
	 *            in seconds
	 */
	public void runFlywheel(double speed, double time) {

	}

	/**
	 * Stops index wheel
	 */
	public void stopIndexWheel() {

	}

	/**
	 * Runs index wheel at a given speed in () for input time in seconds
	 *
	 * @param speed
	 *            in ()
	 * @param time
	 *            in seconds
	 */
	public void runIndexWheel(double speed, double time) {

	}

	/**
	 * Runs fly wheel continuously until ________
	 */
	public void runFlywheelContinuous() {

	}

	/**
	 * Runs index wheel continuously until ________
	 */
	public void runIndexWheelContinuous() {

	}
}
